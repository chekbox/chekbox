const webpack = require('webpack');
const path = require('path');

module.exports = {
  context: __dirname,
  entry: './app/index.jsx',
  output: {
    path: './public/dist/',
    filename: "bundle.js"
  },
  module: {
    loaders: [
      { test: /\.jsx$/, loader: 'babel', exclude: /node_modules/ },
      { test: /\.js$/, loader: 'babel', exclude: /node_modules/, },
      { test: /\.css$/, loader: "style!css" }
    ]
  },
  plugins: [
    new webpack.NoErrorsPlugin()
  ],
  devtool: 'source-map'
};