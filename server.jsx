
var express = require('express');
var exphbs  = require('express-handlebars');
var fs = require('fs');

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

// TODO: Use the proposals page as the root page for now
import ReactDOMServer from 'react-dom/server';
import React from 'react';

// Webpack dev server
// TODO: Only for development purposes
/*var Webpack = require('webpack');
var WebpackDevServer = require("webpack-dev-server");
var webpackConfig = require("./webpack.config");

var compiler = Webpack(webpackConfig);
var server = new WebpackDevServer(compiler, {
  noInfo: false,
  stats: {
    colors: true
  },
  setup: function(app) {
    app.use(function(req, res, next) {
      console.log("Using middleware for " + req.url);
      next();
    });
  }
}); */

app.engine('.hbs', exphbs({extname: '.hbs'}));
app.set('view engine', '.hbs');
app.set('views', './templates/')

app.use(express.static('public'));

import routes from './app/routes';
import { match, RouterContext } from 'react-router';
app.use((req, res) => {
  // Note that req.url here should be the full URL path from
  // the original request, including the query string.
  match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
    if (error) {
      res.status(500).send(error.message)
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search)
    } else if (renderProps) {
      // You can also check renderProps.components or renderProps.routes for
      // your "not found" component or route respectively, and send a 404 as
      // below, if you're using a catch-all route.
      res.status(200).render('layout');
    } else {
      res.status(404).send('Not found')
    }
  })
});

app.use(function(req, res) {
  res.status(400);
  res.render('error', {
    title: '404: File Not Found'
  });
});

app.use(function(error, req, res, next) {
  res.status(500);
  res.render('error', {
    title: '500: Internal Server Error',
    error: error.stack
  });
});

//

var polls = [];
if(fs.existsSync('votes.json')) polls = JSON.parse(fs.readFileSync('votes.json'));
var nextId = (() => {
  var uid = 0;

  polls.forEach((p) => {
    if (p.uid > uid) {
      uid = p.uid;
    }
  });

  return uid + 1;
})();

console.log('next uid=' + nextId);

io.on('connection', (socket) => {
  console.log('A user connected');

  socket.on('create', function(poll){
    console.log('[create] title: ' + poll.title);

    let actual = {uid: nextId, title: poll.title, yeas: 0, nays: 0};
    polls.push(actual);
    nextId++;

    fs.writeFileSync('votes.json', JSON.stringify(polls));

    io.sockets.emit('created', actual);
  });

  socket.on('vote', function(data) {
    let uid = data.uid;
    console.log('[vote] ' + data.direction + ' on ' + uid);

    polls.forEach(function(poll) {
      if(poll.uid == uid) {
        data.direction > 0 ? poll.yeas++ : poll.nays++;

        let votes = poll.yeas - poll.nays;
        let title = poll.title;
        io.sockets.emit('updated', { uid, title, votes })
      }
    });

    fs.writeFileSync('votes.json', JSON.stringify(polls));
  })

  socket.on('fetch', function() {
    polls.forEach(function(poll) {
      socket.emit('created', poll)
    });
  })

  polls.forEach(function(poll) {
    socket.emit('created', poll);
  }, this);
});

io.on('disconnect', (socket) => {
  console.log('A user disconnected');
})

const YEA = 1;
const NAY = -1;
const ABSTAIN = 0;

io.on('vote', (socket, name, id, vote) => {
  console.log(name + ' is voting ' + vote + ' on ' + id);
});

http.listen(3000, () => {
  console.log('Listening on *:3000');
});
