import React from 'react';
import { Link } from 'react-router'

export default function Root({ children }) {
  return (
    <div>
      <div className="top-bar">
        <div className="top-bar-left">
          <ul className="menu">
            <li className="menu-text">chekbox</li>
            <li><Link to="/polls" activeClassName="active">Active Polls</Link></li>
            <li><Link to="/create" activeClassName="active">New Poll</Link></li>
          </ul>
        </div>
      </div>

      {children}
    </div>
  );
}