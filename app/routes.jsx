import React from 'react';
import { Route } from 'react-router'

import Root from './root.jsx';
import Polls from './polls.jsx';
import PollCreate from './poll-create.jsx';

const Routes = (
  <Route path="/" component={Root}>
    <Route path="polls" component={Polls}/>
    <Route path="create" component={PollCreate} /> 
  </Route>
);

export default Routes;