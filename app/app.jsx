import React from 'react';
import { render } from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import routes from './routes.jsx';
import io from 'socket.io-client';

class App extends React.Component {
  constructor() {
    super();
    this.state = { polls: [] };
    window.socket = io(document.domain + ':3000/');
  }

  render() {
    return (
      <Router history={browserHistory}>
        {routes}
      </Router>
    );
  }
}

const instance = <App />;
export default instance;