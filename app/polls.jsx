import React from 'react';

import app from './app.jsx'
import PollItem from './poll-item.jsx'

export default class Polls extends React.Component {

  componentWillMount() {
    // TODO: We need to pass `app` through `Poll`'s props
    //       Probably should lookup how to get a parent properties
    //       You would probably need to see how the `routes.jsx` stuff works
    this.setState({polls: []});

    window.socket.on('created', (poll) => {
      var exists = false;
      this.state.polls.forEach(function(p) {
        if (p.uid == poll.uid) {
          exists = true;
        }
      });

      if (exists) {
        console.log('[created] duplicate uid=' + poll.uid)
      }

      this.state.polls.push(poll);
      console.log(poll);
      this.forceUpdate();
      console.log('[created] title: ' + poll.title);
    });

    window.socket.emit('fetch');
  }

  componentWillUnmount() {
    // TODO: How do we prevent registering the callback mulitple times?
  }

  // TODO: Actually render a updating list of polls
  render() {
    var renderedPolls = [];
    this.state.polls.forEach(function(poll) {
      console.log(poll);
      renderedPolls.push(<PollItem key={poll.uid} uid={poll.uid} title={poll.title} votes={poll.yeas - poll.nays} />);
    }, this);
    return (
      <div className="row column" id="proposals">
        {renderedPolls}
      </div>
    );
  }
}