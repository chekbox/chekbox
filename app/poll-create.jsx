import React from 'react';
import { browserHistory } from 'react-router';
import io from 'socket.io-client';

export default class PollCreate extends React.Component {
  constructor() {
    super();
    this.state = {title: ''};
  }

  handleSubmit() {
    console.log('[create] title: ' + this.state.title)
    window.socket.emit('create', { title: this.state.title })
    browserHistory.push('/polls');
  }

  handleChange(event) {
    this.setState({title: event.target.value});
  }

  render() {
    return (
      <div className="row column">
        <h1>Create Poll</h1>

        <form>
          <div className="row">
            <div className="large-12 columns">
              <label>Title
                <input type="text" value={this.state.title} onChange={this.handleChange.bind(this)} />
              </label>
            </div>
          </div>

          <a onClick={this.handleSubmit.bind(this)} className="button">Create</a>
        </form>
      </div>
    );
  }
}