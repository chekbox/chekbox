import React from 'react'

export default class PollItem extends React.Component {

  constructor(props) {
    super(props);

    this.state = {}
    this.state.title = props.title;
    this.state.votes = props.votes;
  }

  componentWillMount() {
    window.socket.on('updated', (newPoll) => {
      let uid = this.props.uid;

      console.log('[update] begin [old = ' + uid + '] [new = ' + newPoll.uid + ']')
      console.log(newPoll);
      
      if (newPoll.uid != uid) return;

      this.state.title = newPoll.title;
      this.state.votes = newPoll.votes;
      console.log(this.state);

      console.log('[updated] title: ' + newPoll.title);
      this.forceUpdate();
    })
  }

  onVoteYea() {
    window.socket.emit('vote', {uid: this.props.uid, direction: 1})
  }

  onVoteNay() {
    window.socket.emit('vote', {uid: this.props.uid, direction: -1})
  }

  render() {
    let uid = this.props.uid;
    let title = this.state.title;
    let votes = this.state.votes;

    return (
      <div className="primary callout">
        <div className="row proposal">
          <div className="small-2 columns">
            <button type="button" className="secondary expanded button vote-yea" onClick={this.onVoteYea.bind(this)}>
              <i className="fi-check"></i>
            </button>

            <p className="text-center stat vote-count">{votes}</p>

            <button type="button" className="secondary expanded button vote-nay" onClick={this.onVoteNay.bind(this)}>
              <i className="fi-x"></i>
            </button>
          </div>
          <div className="small-10 columns">
            <em>Proposal #<span className="proposal-id">{uid}</span></em>
            <h1 className="proposal-title">{title}</h1>
          </div>
        </div>
      </div>
    );
  }
}