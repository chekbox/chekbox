import React from 'react'
import { render } from 'react-dom'

import app from './app.jsx'
import routes from './routes.jsx';

render(app, document.getElementById('app'));
